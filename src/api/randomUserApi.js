/* eslint-disable prettier/prettier */
import axios from "axios";

export async function getRandomUsers(gender, nationality, params = "?", results = 100) {
  params += `results=${results}&`;
  if (gender) {
    params += `gender=${gender}&`;
  }
  if (nationality) {
    params += `nat=${nationality.join()}&`;
  }
  // if (page) {
  //   params += `page=${page}`
  // }

  try {
    let response = await axios.get(`https://randomuser.me/api/${params}`);

    if (response.data) {
      return response.data;
    } else {
      return [];
    }
  } catch (error) {
    console.log(error);
  }
}
