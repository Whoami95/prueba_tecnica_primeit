const getters = {
  usersSelected: (state) => state.usersSelected,
  users: (state) => state.users,
  userProfile: (state) => state.userProfile,
};
export default getters;
