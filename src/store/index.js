import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    users: [],
    usersSelected: [],
    userProfile: null,
  },
  mutations: {
    SET_USERS: (state, users) => {
      state.users = users;
    },
    DELETE_USERS: (state) => {
      state.users = [];
    },
    SET_USER_PROFILE: (state, userProfile) => {
      state.userProfile = userProfile;
    },
    DELETE_USER_PROFILE: (state) => {
      state.userProfile = null;
    },
    SET_USERS_SELECTED: (state, usersSelected) => {
      state.usersSelected = usersSelected;
    },
    DELETE_USERS_SELECTED: (state) => {
      state.usersSelected = [];
    },
  },
  actions: {
    changeUserProfile({ commit }, user) {
      commit("DELETE_USER_PROFILE");
      commit("SET_USER_PROFILE", user);
    },
    changeUsers({ commit }, users) {
      commit("DELETE_USERS");
      commit("SET_USERS", users);
    },
    changeUsersSelected({ commit }, usersSelected) {
      commit("DELETE_USERS_SELECTED");
      commit("SET_USERS_SELECTED", usersSelected);
    },
  },

  modules: {},
  getters,
});
export default store;
