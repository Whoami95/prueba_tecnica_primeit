import Vue from "vue";
import VueRouter from "vue-router";
import UsersList from "../views/UsersList.vue";
import Export from "../views/Export.vue";
import Profile from "../views/Profile.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "UsersListRoute",
    component: UsersList,
  },
  {
    path: "/export",
    name: "ExportUsersRoute",
    component: Export,
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
