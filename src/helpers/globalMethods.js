import store from "@/store";

export default {
  install: (Vue) => {
    Vue.prototype.$helpers = {
      getUsers() {
        return store.getters.users;
      },
      getUserProfile() {
        return store.getters.userProfile;
      },
      getUsersSelected() {
        return store.getters.usersSelected;
      },
    };
  },
};
