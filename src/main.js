import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import axios from "axios";
import VueAxios from "vue-axios";
import CountryFlag from "vue-country-flag";
import JsonCSV from "vue-json-csv";
import moment from "moment";
import "moment/locale/en-gb";
import "./registerServiceWorker";
import globalMethods from "./helpers/globalMethods";
import { LMap, LTileLayer, LMarker } from "vue2-leaflet";
import "leaflet/dist/leaflet.css";
import { Icon } from "leaflet";

Vue.use(globalMethods);

Vue.component("downloadCsv", JsonCSV);
Vue.component("vue-country-flag", CountryFlag);

Vue.use(VueAxios, axios);
Vue.config.productionTip = false;

// MomentJs
moment.locale("en-gb");
Vue.prototype.$moment = moment;

// global bus
const EventBus = new Vue();
Vue.prototype.$bus = EventBus;

// Leaflet map
Vue.component("l-map", LMap);
Vue.component("l-tile-layer", LTileLayer);
Vue.component("l-marker", LMarker);
delete Icon.Default.prototype._getIconUrl;
Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
});

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
